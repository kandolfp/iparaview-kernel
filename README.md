# IParaView Jupyter Kernel

Copyright (c) 2021 Kitware SAS
All rights reserved.

IParaView-Kernel is a free software; you can redistribute it and/or modify it
under the terms of the IParaView-Kernel license.

See License.txt for the full IParaView-Kernel license.

## How to install

This plugin has 3 dependencies :

 - *ParaView* with Qt support (see https://gitlab.kitware.com/paraview/paraview). Compatible with ParaView 5.11 and above.
 - *ipykernel* (`ipykernel` pip package). Only ipykernel 6.4.1 has been tested so far. This project should at least be compatible with ipykernel 6.0 and onwards.
 - *IPython* (see https://ipython.org/install.html). Only IPython 7.29 has been tested so far. This project should at least be compatible with IPython 7.0 and onwards.

This plugin also provides tools for displaying render views directly in the jupyter notebook using a dedicated widget.
To use this (optional) feature, this additionnal dependency is needed:

 - *ipyvtklink* (`ipyvtklink` pip package)

**Note**: for now, *ipyvtklink* only supports *ipywidgets ver. 7.7.X*. Installation or downgrading of *ipywidget* to a compatible version
should be automatic when *ipyvtklink* is installed with pip.

Since this project is a Jupyter kernel, a jupyter client will also need to be installed to be able to use this kernel.
Only Jupyter Notebook has been tested for now.

Once these dependencies are installed, create a build directory and build the project using CMake like a regular C++ project.
Important CMake variables :

 - `ParaView_DIR` : the build/install folder of the ParaView this plugin has to work with.
 - `CMAKE_INSTALL_PREFIX` : where to install the kernel.
See https://jupyter-client.readthedocs.io/en/stable/kernels.html#kernel-specs for the list of the locations jupyter will look by default.

### Important

This project **must** be installed so it can work correctly out of the box (via `cmake --build <build-folder> --target install` for example).

## How to use

 - Install the kernel
 - Launch an IPython interface.For example if you work with the notebook interface : `jupyter notebook`
 - Create a new project and choose the kernel named `IParaView Kernel`

This jupyter kernel will try to launch a `pvserver` on `localhost` at a random port between 11000 and 50000.
Note that it is possible to control the port range using the environment variable `IPARAVIEW_PORT_RANGE`. A usage example is :

```
export IPARAVIEW_PORT_RANGE=10000,11000
```

Note that if you want to use a specific port it is possible via `export IPARAVIEW_PORT_RANGE=10000,10000`.
Also note that an incorrect `IPARAVIEW_PORT_RANGE` syntax will make the kernel ignore this setting.
If the ParaView server cannot launch then kernel will fail.

Once the server is launched, the interpreter will connect to the server at startup and directly setup the environment so you can right away type some pvpython commands.
To have more information on what is available specifically through this kernel, type magic command `%help` in the interpreter.

For a basic example of the kernel using Jupyter Notebook, see `Examples/simpleDemo.ipynb`.

## Test environment with docker

Included you find a `Dockerfile` with a simple test environment based on JupyterLab. 
The installation is based on *Python 3.9* and uses *miniconda* to install Paraview 5.11 (build `py39h03a2555_101_qt` on `conda-forge`).

To build the container run:
```bash
$ docker build -t iparaview_kernel_test .
```
and to run it:
```bash
$ docker run -it -p 8888:8888 iparaview_kernel_test
```
You will see the URL showing up to connect to the JupyterLab.
The container starts in the `./Examples` folder.
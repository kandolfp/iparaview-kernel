FROM ubuntu:20.04 as build

# Jupyter part
WORKDIR /root

# Fix: https://github.com/hadolint/hadolint/wiki/DL4006
# Fix: https://github.com/koalaman/shellcheck/wiki/SC3014
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

USER root

# Install all OS dependencies 
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update --yes && \
    apt-get upgrade --yes && \
    apt-get install --yes --no-install-recommends \
    tini \
    locales \
    wget \
    ca-certificates \
    # dependencies for IParaView Jupyter Kernel that can not be installed with conda
    gcc \
    g++ \
    gfortran \
    make \
    cmake \
    xvfb \
    libgl1-mesa-dev \
    libglu1-mesa-dev \
    mesa-common-dev \
    # the next three are dependencies for the vtk package that is otherwise not installed correct
    ffmpeg \
    libsm6 \
    libxext6 && \
    apt-get clean && rm -rf /var/lib/apt/lists/* && \
    echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen

RUN wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ./miniconda.sh && \
     /bin/bash ./miniconda.sh -b -p /opt/conda

# Configure environment
ENV CONDA_DIR=/opt/conda \
    SHELL=/bin/bash \
    LC_ALL=en_US.UTF-8 \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8
ENV PATH="${CONDA_DIR}/bin:${PATH}"

# Pin python version here, or set it to "default"
ARG PYTHON_VERSION=3.9

RUN apt-get update && \
    apt-get upgrade -y

ARG PARAVIEW_VERSION=5.11.0
ARG PARAVIEW_BUILD=py39h03a2555_101_qt

RUN conda install -c conda-forge python="${PYTHON_VERSION}" \
       paraview="${PARAVIEW_VERSION}"="${PARAVIEW_BUILD}" \
       boost=1.78.0 \
       cli11 \
       nlohmann_json \
       eigen \
       notebook \
       jupyterlab \
       jupyterhub \
       ipyvtklink

RUN jupyter notebook --generate-config && \
    conda clean -a -y && \
    jupyter lab clean

ENV ParaView_DIR=/home/jovyan/.conda/pkgs/paraview-{PARAVIEW_VERSION}"-"${PARAVIEW_BUILD}" \
    CMAKE_INSTALL_PREFIX=/home/jovyan/.local/share/jupyter/kernels \
    PYTHONPATH=/home/jovyan/.conda/pkgs/paraview-${PARAVIEW_VERSION}"-"${PARAVIEW_BUILD}"/lib:/home/jovyan/.conda/pkgs/paraview-{PARAVIEW_VERSION}"-"${PARAVIEW_BUILD}"/lib/python3.9/site-packages \
    LD_LIBRARY_PATH=/home/jovyan/.conda/pkgs/paraview-{PARAVIEW_VERSION}"-"${PARAVIEW_BUILD}"/lib:/home/jovyan/.conda/envs/paraview/lib \
    PATH=/opt/conda/bin:/opt/conda/condabin:/opt/conda/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

COPY . ./iparaview-kernel

RUN cd iparaview-kernel/ && ls && cmake -S . && cmake --build . --target install

EXPOSE 8888

# Configure container startup
COPY start_xvfb.sh /sbin/start_xvfb.sh
RUN chmod a+x /sbin/start_xvfb.sh

ENV XDG_RUNTIME_DIR=/tmp/runtime-root

ENTRYPOINT ["tini", "-g", "--", "start_xvfb.sh"]
CMD ["jupyter", "lab", "--allow-root", "--ip=0.0.0.0"]

WORKDIR "/root/iparaview-kernel/Examples"